#! /usr/bin/bash

docker-compose exec django_asset_manager.com python manage.py flush --no-input
docker-compose exec django_asset_manager.com python manage.py migrate
docker-compose exec django_asset_manager.com python manage.py loaddata /dev_data/tracked_items.yaml
docker-compose exec django_asset_manager.com python manage.py loaddata /dev_data/auth.yaml
