# README #

Django-asset-manager is an open-source project intended to assist with tracking inventory transactions and
inventory verification at Illinois State University. Django-asset-manager is currently pre-release.
### How do I get set up? ###

* Run docker-compose up
* Dev data is provided in the root of the repo under 'dev-data' A custom command it provided to load dev data,
  run 'manage.py loaddevdata'.
* To run tests, run 'manage.py test'
* Access the admin app at /localhost:3000/admin/
* Access the front-end application at localhost:3000


### Contribution guidelines ###

* Writing tests
    
    Test driven design in encouraged. Ideally, feature branches include a functional test which verifies the user story. 
    Additionally, any code which implements business logic should be unit-tested.
* Code review

    Peer review is essential to code quality. Please do not merge or commit directly to master.
    Create a pull request and notify a team member for review.
* Other guidelines
    * Update dev_data as needed if you create models.
    * Use Jira's branch naming conventions; feature branches for user story issued, bug fix for bug branches, etc...
    Additional branch types may be made available as the project evolves.
   
### Who do I talk to? ###

* This project is maintained by Chris Morgan, Director of Technology, Mennonite College of Nursing.
  He can be reached at crmorga@ilstu.edu
* Other other prominent contributors include:
    * Matthew Rutherford
    * Ramsey Barghouti