from django.contrib import admin
from core import models
from reversion.admin import VersionAdmin
# Register your models here.


@admin.register(models.TrackedItem)
class TrackedItemAdmin(VersionAdmin):
    pass


@admin.register(models.Customer)
class CustomerItemAdmin(VersionAdmin):
    pass


@admin.register(models.EquipmentLoanRequest)
class EquipmentLoanRequest(VersionAdmin):
    pass
