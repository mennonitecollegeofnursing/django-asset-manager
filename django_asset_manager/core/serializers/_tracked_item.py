from rest_framework import serializers
from core.models import TrackedItem
from core.serializers.validators import NoUpdateValidator


class TrackedItemSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = TrackedItem
        fields = (
            'url',
            'id',
            'asset_tag',
            'department',
            'building',
            'room',
            'description',
            'serial_number',
            'cost',
            'date',
            'property_control_notes',
            'comments',
            'item_image',
            'purchase_date',
            'item_type'
        )
        validators = [
            NoUpdateValidator(
                fields=[
                    "asset_tag",
                    "serial_number",
                    "cost",
                    "purchase_date"
                ]
            )
        ]
