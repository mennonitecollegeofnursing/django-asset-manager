from rest_framework import serializers
from core.models import EquipmentLoanRequest


class EquipmentLoanRequestSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = EquipmentLoanRequest
        fields = (
            'url',
            'id',
            'tracked_item',
            'customer',
            'loan_start_date',
            'loan_end_date',
            'loan_return_date',
            'location',
            'approver_name',
            'department_name',
            'borrower_signature',
            'approval_signature',
            'return_verification_signature'
        )
