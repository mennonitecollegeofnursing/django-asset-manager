from rest_framework import serializers
from core.models import Customer


class CustomerSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Customer
        fields = (
            'url',
            'id',
            'email',
            'first_name',
            'last_name',
            'office_phone',
            'home_phone',
            'mobile_phone',
            'office_number',
            'building',
        )
