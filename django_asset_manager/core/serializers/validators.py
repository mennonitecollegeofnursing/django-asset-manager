from rest_framework.exceptions import ValidationError


class NoUpdateValidator:
    """Raise validation error if serializer attempts to update specified fields
    after the creation time"""
    requires_context = True

    def __init__(self, fields):
        self.fields = fields

    def __call__(self, value, serializer):
        is_update = serializer.instance is not None
        if is_update:
            for field_name in self.fields:
                if field_name in value.keys():
                    field_value = value.get(field_name)
                    instance_value = getattr(serializer.instance, field_name)
                    if instance_value != field_value:
                        raise ValidationError(
                            "You may not update %s after it is created" % field_name)
