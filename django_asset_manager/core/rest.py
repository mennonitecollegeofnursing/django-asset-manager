from rest_framework.routers import DefaultRouter

from core import viewsets as vs

router = DefaultRouter()

router.register('customers', vs.CustomerViewSet)
router.register('equipment_loan_requests', vs.EquipmentLoanRequestViewSet)
router.register('tracked_items', vs.TrackedItemViewSet)
