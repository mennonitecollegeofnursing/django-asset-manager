from django.core.management.base import BaseCommand, CommandError
from django.core.management import call_command
from django.contrib.auth.models import User
import django_asset_manager.settings as settings



class Command(BaseCommand):
    help = 'Only runs in debug mode. Flushes DB,Loads dev data, and creates default super user account.'

    def handle(self, *args, **options):
        if settings.DEBUG:
            call_command('flush')
            call_command('loaddata', '/dev_data/tracked_items')
            user = User.objects.create_superuser(username="root", email='root@example.com',
                                                 password="password", is_active=True)
            user.save()
            self.stdout.write(self.style.SUCCESS('Successfully created root with password password'))
        else:
            raise CommandError('DEBUG is False.  Edit your settings to enable this command.')