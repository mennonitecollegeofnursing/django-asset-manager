from rest_framework import viewsets

from core.models import TrackedItem
from core.serializers import TrackedItemSerializer


class TrackedItemViewSet(viewsets.ModelViewSet):
    queryset = TrackedItem.objects.all()
    serializer_class = TrackedItemSerializer
