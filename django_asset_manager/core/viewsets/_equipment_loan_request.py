from rest_framework import viewsets

from core.models import EquipmentLoanRequest
from core.serializers import EquipmentLoanRequestSerializer


class EquipmentLoanRequestViewSet(viewsets.ModelViewSet):
    queryset = EquipmentLoanRequest.objects.all()
    serializer_class = EquipmentLoanRequestSerializer
