from django.test import TestCase
from core.models import (
    Customer,
    TrackedItem,
)
from django.db.migrations.serializer import FunctionTypeSerializer
from django.db import models as dj_models

# Create your tests here.


class TrackedItemTest(TestCase):

    def setUp(self):
        TrackedItem.objects.create(
            asset_tag='123456',
            department='ABC',
            building='Test Hall',
            room='123',
            description='COMPUTER, LAPTOP, LATITUDE 7490',
            serial_number='9Y23RV2',
            cost='1,061.73',
            date='12/31/2020',
            property_control_notes="Testy's laptop",
            comments='Out for service',
            item_image='ABC',
            purchase_date='6/4/2019',
            item_type='laptop'
        )
        Customer.objects.create(
            email='test@examle.com',
            first_name='Testy',
            last_name='McTestface',
            office_phone='123-456-7890',
            home_phone='123-456-7890',
            mobile_phone='123-456-7890',
            office_number='123',
            building='Test Hall'
        )

    def test_tracked_item(self):
        tracked_item = TrackedItem.objects.get(asset_tag='123456')
        self.assertIsNotNone(tracked_item)

    def test_customer(self):
        customer = Customer.objects.get(email='test@examle.com')
        self.assertIsNotNone(customer)