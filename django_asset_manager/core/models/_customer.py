from django.db import models


# Create your models here.

class Customer(models.Model):
    email = models.EmailField(max_length=256)
    first_name = models.CharField(max_length=64)
    last_name = models.CharField(max_length=64)
    office_phone = models.CharField(max_length=12)
    home_phone = models.CharField(max_length=12)
    mobile_phone = models.CharField(max_length=12)
    office_number = models.CharField(max_length=256)
    building = models.CharField(max_length=256)
