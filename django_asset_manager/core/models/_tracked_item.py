from django.db import models


# Create your models here.
class TrackedItem(models.Model):

    asset_tag = models.CharField(max_length=6)
    department = models.CharField(max_length=256)
    building = models.CharField(max_length=256)
    room = models.CharField(max_length=256)
    description = models.CharField(max_length=2048)
    serial_number = models.CharField(max_length=256)
    cost = models.CharField(max_length=256)
    date = models.CharField(max_length=10)
    property_control_notes = models.CharField(max_length=2048)
    comments = models.CharField(max_length=2048)
    item_image = models.ImageField()
    purchase_date = models.CharField(max_length=10)
    item_type = models.CharField(max_length=256)
