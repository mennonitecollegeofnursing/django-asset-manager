from django.db import models
from core.models._tracked_item import TrackedItem
from core.models._customer import Customer


# Create your models here.

class EquipmentLoanRequest(models.Model):
    tracked_item = models.ForeignKey(TrackedItem, on_delete=models.PROTECT)
    customer = models.ForeignKey(Customer, on_delete=models.PROTECT)

    loan_start_date = models.DateField(null=True, blank=True)
    loan_end_date = models.DateField(null=True, blank=True)
    loan_return_date = models.DateField(null=True, blank=True)

    location = models.TextField(null=True, blank=True)
    approver_name = models.CharField(null=True, blank=True, max_length=128)
    department_name = models.CharField(null=True, blank=True, max_length=128)

    borrower_signature = models.ImageField(null=True, blank=True)
    approval_signature = models.ImageField(null=True, blank=True)
    return_verification_signature = models.ImageField(null=True, blank=True)
