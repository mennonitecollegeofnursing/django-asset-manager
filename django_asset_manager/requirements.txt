asgiref==3.3.1
Django==3.1.4
django-reversion==3.0.8
djangorestframework==3.12.2
Pillow==8.0.1
pytz==2020.5
PyYAML==5.3.1
sqlparse==0.4.1 -r rea
psycopg2==2.8.6
